export const AUTH_USER = 'AUTH_USER';
export const FAIL_LOGIN = 'FAIL_LOGIN';
export const GET_CHART_INFORMATION = 'GET_CHART_INFORMATION';
export const GET_LOGIN = 'GET_LOGIN';
export const GET_USER_BY_ID = 'GET_USER_BY_ID';
export const GET_USERS = 'GET_USERS';
export const GET_USERS_GOOGLE = 'GET_USERS_GOOGLE';
export const REDIRECT_LOG_OUT_USER = 'REDIRECT_LOG_OUT_USER';
export const REGISTER_FAIL = 'REGISTER_FAIL';
export const UNAUTH_USER = 'UNAUTH_USER';