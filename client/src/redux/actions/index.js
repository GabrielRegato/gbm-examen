import {
  AUTH_USER,
  FAIL_LOGIN,
  GET_CHART_INFORMATION,
  GET_LOGIN,
  GET_USER_BY_ID,
  GET_USERS,
  GET_USERS_GOOGLE,
  REDIRECT_LOG_OUT_USER,
  REGISTER_FAIL,
  UNAUTH_USER,
} from '../constants/action-types';

import api from "../../helpers/api";

import {
  saveToken,
  removeToken,
  getUserInfo,
  parseToken,
  formatErrors
} from "../../helpers/helper";

export function getChartInformation() {
	return function action(dispatch) {
		const request = fetch('/chartsInfo').then((response) => {
    		return response.json();
  		});

  		return request.then(json => {
    		dispatch(chartInformation(json));
  		});
	}
}

export function postLogin({ user, pass }) {
	return function action(dispatch) {
		fetch('/login', { method: "POST",body : JSON.stringify({
			user, pass
		})}).then((response) => {
			if (response.status !== 200) {
				return dispatch(getlogin(response.status));
			} else {
				return dispatch(getlogin(response));
			}
  		});
	}
}

export function getUserById(idUser) {
  return function action(dispatch) {
    const request = fetch(`/userById?uid=${idUser}`).then((response) => {
        return response.json();
      });

      return request.then(json => {
        dispatch(getUserInformation(json));
      });
  }
}

export function getUsers() {
	return function action(dispatch) {
		const request = fetch('/listUsers').then((response) => {
    		return response.json();
  		});

  		return request.then(json => {
    		dispatch(getAllUsers(json));
  		});
	}
}

export function getUsersGoogle() {
	return function action(dispatch) {
		const request = fetch('/listUsersGoogle').then((response) => {
    		return response.json();
  		});

  		return request.then(json => {
    		dispatch(getAllUsersGoogle(json));
  		});
	}
}
export const loginUser = (userInfo, redirect) => async dispatch => {
  try {
    let { data } = await api.login(userInfo);
    saveToken(data.token);
    dispatch(authUser(data.userInfo));
    redirect();
  } catch (e) {
    if (!e.response) {
      console.log(e);
      return;
    }
    let { data } = e.response;
    dispatch(loginFail(data));
  }
};

export const reAuthUser = () => async dispatch => {
  const userInfo = getUserInfo();
  if (userInfo && userInfo.exp >= Date.now() / 1000) {
    dispatch(authUser(userInfo));
  } else {
    dispatch(redirectLogOutUser());
  }
};

export const socialAuthUser = (token, redirect) => async dispatch => {
  saveToken(token);
  dispatch(authUser(parseToken(token)));
  redirect();
};

export const registerUser = (userInfo, redirect) => async dispatch => {
  try {
    let { data } = await api.register(userInfo);
    saveToken(data.token);
    dispatch(authUser(data.userInfo));
    redirect();
  } catch (e) {
    if (!e.response) {
      console.log(e);
      return;
    }
    let { data } = e.response;
    const formattedErrors = formatErrors(data);
    dispatch(registerFail(formattedErrors));
  }
};

export const logoutUser = redirect => async dispatch => {
  removeToken();
  dispatch(unAuthUser());
  redirect();
};

export const chartInformation = json => ({
	type: GET_CHART_INFORMATION,
	payload: json,
});

export const getlogin = json => ({
	type: GET_LOGIN,
	payload: json,
});

export const getAllUsers = json => ({
	type: GET_USERS,
	payload: json,
});

export const getAllUsersGoogle = json => ({
	type: GET_USERS_GOOGLE,
	payload: json,
});
export const authUser = userInfo => {
  return {
    type: AUTH_USER,
    userInfo,
  };
};

export const unAuthUser = () => {
	return {
  		type: UNAUTH_USER,
  	};
};

export const redirectLogOutUser = () => {
  return {
      type: REDIRECT_LOG_OUT_USER,
    };
};

export const loginFail = errors => {
  return {
    type: FAIL_LOGIN,
    errors,
  };
};

export const registerFail = errors => {
  return {
    type: REGISTER_FAIL,
    errors,
  };
};

export const getUserInformation = json => ({
  type: GET_USER_BY_ID,
  userInformation: json,
});