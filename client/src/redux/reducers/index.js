import { combineReducers } from 'redux';
import chartInfo from './chartInformation';
import getLogin from './getLogin';
import Users from './getUsers';
import UsersGoogle from './getUsersGoogle';

export default combineReducers({
	chartInfo,
	getLogin,
	Users,
	UsersGoogle,
});
