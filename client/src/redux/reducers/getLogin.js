import { GET_LOGIN, FAIL_LOGIN, AUTH_USER, UNAUTH_USER, REGISTER_FAIL, REDIRECT_LOG_OUT_USER } from '../constants/action-types';

const initialState = {
	isAuthed: false,
	registerErrors: {},
	login: [],
	loginFail: {},
  	userInfo: {},
  	loginErrors: {},
  	redirectLogOutUser: false,
};

const login = (state = initialState, action) => {
	switch (action.type) {
	 	case GET_LOGIN:
			return {
				...state,
				login: [ ...state.login, action.payload ],
			};
		case FAIL_LOGIN:
		  return {
		  	...state,
		  	loginFail: action.errors,
		  };
		case AUTH_USER:
		  return {
		  	...state,
        	isAuthed: true,
        	userInfo: action.userInfo,
		  	redirectLogOutUser: false,
		  };
		case UNAUTH_USER:
		return initialState;
		case REDIRECT_LOG_OUT_USER:
			return {
				...state,
				redirectLogOutUser: true,
			};
		case REGISTER_FAIL:
		return {
 			...state,
        	registerErrors: action.errors
		};
	 	default:
		return state;
	}
};



export default login;