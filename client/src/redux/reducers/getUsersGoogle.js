import { GET_USERS_GOOGLE } from '../constants/action-types';

const initialState = {
	usersGoogle: [],
};

const usersGoogle = (state = initialState, action) => {
	switch (action.type) {
		case GET_USERS_GOOGLE:
		  state.usersGoogle = [];
		  return { ...state, usersGoogle: [...state.usersGoogle, action.payload] };
	  default:
		return state;
	}
  };

export default usersGoogle;