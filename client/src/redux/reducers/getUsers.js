import { GET_USERS, GET_USER_BY_ID } from '../constants/action-types';

const initialState = {
	users: [],
	userById: {},
};

const users = (state = initialState, action) => {
	switch (action.type) {
		case GET_USERS:
		  return { ...state, users: [...state.users, action.payload] };
 		case GET_USER_BY_ID:
 		return {
			...state,
			userById: action.userInformation,
		};
	  default:
		return state;
	}
  };

export default users;