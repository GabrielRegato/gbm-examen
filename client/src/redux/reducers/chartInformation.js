import { GET_CHART_INFORMATION } from '../constants/action-types';

const initialState = {
	chart: [],
};

const chart = (state = initialState, action) => {
	switch (action.type) {
	  case GET_CHART_INFORMATION:
		  return { ...state, chart: [...state.chart, action.payload] };
	  default:
		return state;
	}
  };

export default chart;