import React from 'react';
import Img from '../img/404.png';

const NotFound = () => {
    return(
        <div className='not-found'>
            <span className='not-found-text-expredion' >!Ups.</span>
            <img className='not-found-img' alt='404' src={ Img } />
            <span className='not-found-text' >Lo sentimos, no existe esta ruta.</span>
            <div className='not-found-bar'></div>
        </div>
    );
}

export default NotFound;