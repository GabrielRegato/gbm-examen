import React, { Component, Fragment } from 'react';
import Header from '../components/global/header';
import Menu from '../components/menu';
import User from '../components/users';

import { connect } from "react-redux";

const mapStateToProps = state => ({
  userInfo: state.getLogin.userInfo,
});

class Users extends Component {

    componentWillReceiveProps(nextProps) {
        const {
            userInfo,
            history,
        } = this.props;

        if ( userInfo !== nextProps.userInfo ) {
            if ( nextProps.userInfo && nextProps.userInfo.userType && nextProps.userInfo.userType !== 1 ){
                history.push('/home');
            }
        }
    }

    render() {
        return (
            <Fragment>
                <Header { ...this.props } />
                <div className='dashBoard'>
                    <div className='dashBoard-menu'>
                        <Menu { ...this.props } />
                    </div>
                    <div className='dashBoard-content'>
                        <User {...this.props} />
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default connect(mapStateToProps)(Users);
