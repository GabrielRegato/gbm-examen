import React, { Component, Fragment } from 'react';
import Btn from '../components/global/Button';
import cx from 'classnames';
import Input from '../components/global/input';
import { connect } from 'react-redux';
import { reAuthUser, loginUser } from "../redux/actions/index";

const mapStateToProps = state => ({
  login : state.getLogin.login,
  loginFail: state.getLogin.loginFail,
  userInfo: state.getLogin.userInfo,
});

class Login extends Component {
  state = {
    activeBtn: false,
  }

  componentWillMount() {
    const {
      dispatch,
    } = this.props;
    dispatch(reAuthUser());
  }

  componentWillReceiveProps(nextProps) {
    const {
      userInfo,
      history,
    } = this.props;

    if ( userInfo !== nextProps.userInfo ) {
      history.push('/home');
    }
  }

  render() {
    const {
      hostname,
      origin,
    } = window.location;

    const googleURL = `http://${hostname}:3002/auth/google?linkinguri=${origin}/socialauthredirect`;

    const { activeBtn } = this.state;
    const { loginFail } = this.props;

    const errorEmail = loginFail && loginFail.email ? true : false;
    const errorPassword = loginFail && loginFail.password ? true: false;

    const active = cx({
      'Btn': true,
      'Btn-inactive' : !activeBtn,
    });

    const emailClass = cx({
      'inputSession': true,
      'log-in-inputFail': errorEmail,
    });

    const passwordClass = cx({
      'inputSession': true,
      'log-in-inputFail': errorPassword,
    });

    return (
      <Fragment>
        <div className="log-in">
          <div className='log-in-wrap'>
              <h2>Inicio de sesión</h2>
              <form  className='log-in-form' onSubmit={this.handleFormSubmit}>
                <Input
                  className={ emailClass }
                  onKeyUp={ this.validateInput }
                  ref='contentUser'
                  reference='user'
                  title='Correo Electrónico'
                  type='email' />
                  { errorEmail ?
                  <p className='log-in-error-input'>Correo Electrónico Incorrecto</p> : null}
                <Input
                  className={ passwordClass }
                  onKeyUp={ this.validateInput }
                  ref='contentPass'
                  reference='pass'
                  title='Contraseña'
                  type='password' />
                  { errorPassword ?
                  <p className='log-in-error-input'>Contraseña Incorrecto</p> : null }
                <input
                  type="submit"
                  value="INICIAR SESIÓN"
                  className={ active }
                  disabled={ !activeBtn }
                />
              </form>
              <div className='log-in-other'>
                <p className='log-in-title-other'>O</p>
              </div>
              <a
                className='Btn Btn-google'
                href={ googleURL }
              >
              INICIA CON GOOGLE
              </a>
              <div className='log-in-other log-in-register'>
                <p>¿No tienes cuenta?</p>
                <Btn
                  text='REGÍSTRATE'
                  className='Btn-default'
                  onClick={ this.register } />
              </div>
          </div>
        </div>
      </Fragment>
    );
  }

  handleFormSubmit = async e => {
    e.preventDefault();
    const {
      dispatch,
      history,
      location: {
        state,
      },
    } = this.props;

    const payload = {
      email : this.refs.contentUser.refs.user.value,
      password : this.refs.contentPass.refs.pass.value,
    };

    dispatch(loginUser(payload, () => {
      const { from } = state || { from: { pathname: "/home" }};

      history.push(from.pathname);
    }));
  };

  validateInput = () => {
    let validate = null;
    if (this.refs.contentUser.refs.user.value && this.refs.contentPass.refs.pass.value) {
      validate = true;
    } else {
      validate = false;
    }
    this.setState({
      activeBtn : validate,
    });
  }

  register = () => {
    const { history } = this.props;
    history.push('/registro')
  }
}

export default connect(mapStateToProps)(Login);