import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Input from '../components/global/input';

import Header from '../components/global/header';
import Menu from '../components/menu';

const mapStateToProps = state => ({
  userInfo: state.getLogin.userInfo,
});

class Profile extends Component {
  state = {
    code: "",
  };

  render() {
    const {
      userInfo,
    } = this.props;

    return (
      <Fragment>
        <Header { ...this.props } />
        <div className='dashBoard'>
          <div className='dashBoard-menu'>
            <Menu { ...this.props } />
          </div>
          <div className='dashBoard-content'>
            <h1>Mi cuenta</h1>
            <form onSubmit={this.handleFormSubmit} className='register-form'>
              <Input
                className='inputSession'
                title='Nombre Completo'
                type='text'
                disabled={ true }
                value={ userInfo && userInfo.name ? userInfo.name : '' } />
              <Input
                className='inputSession'
                title='Correo Electrónico'
                type='text'
                disabled={ true }
                value={ userInfo && userInfo.email ? userInfo.email : '' } />
              <Input
                className='inputSession'
                title='Tipo de Usuario'
                type='text'
                disabled={ true }
                value={ userInfo && userInfo.userType && userInfo.userType === '1' ? 'Administrador' : 'Usuario' } />
              <Input
                className='inputSession'
                title='Genero'
                type='text'
                disabled={ true }
                value={ userInfo && userInfo.gender && userInfo.gender === 'male' ? 'Hombre' : 'Mujer' } />
          </form>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps)(Profile);
