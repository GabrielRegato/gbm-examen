import React, { Component, Fragment } from 'react';
import DashBoard from '../components/dashBoard';
import Header from '../components/global/header';
import Menu from '../components/menu';
import { connect } from "react-redux";

const mapStateToProps = state => ({
  userInfo: state.getLogin.userInfo,
});

class Charts extends Component {
    render() {
        return (
            <Fragment>
                <Header { ...this.props } />
                <div className='dashBoard'>
                    <div className='dashBoard-menu'>
                        <Menu { ...this.props } />
                    </div>
                    <div className='dashBoard-content'>
                        <DashBoard/>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default connect(mapStateToProps)(Charts);
