import React, { Component } from 'react';
import { connect } from 'react-redux';

import Btn from '../components/global/Button';
import cx from 'classnames';
import Input from '../components/global/input';
import RadioBtn from '../components/global/RadioBtn';
import { registerUser } from "../redux/actions/index";

const mapStateToProps = state => ({});

class Register extends Component {
    state = {
      activeBtn: false,
      register: false,
      checkValue : null,
    }

    render() {
        const {
          activeBtn,
        } = this.state;

        const active = cx({
            'Btn': true,
            'Btn-inactive' : !activeBtn,
        });

        const obj = [
            {
              text: 'Hombre',
            },
            {
              text: 'Mujer',
            },
        ];

        return (
            <div className="register">
                <div className='register-wrap'>
                    <div className='register-content'>
                        <h1>Registro</h1>
                        <form onSubmit={this.handleFormSubmit} className='register-form'>
                            <Input
                                className='inputSession'
                                onKeyUp={ this.validate }
                                ref='contentNewUser'
                                reference='newUser'
                                title='Nombre Completo'
                                type='text' />
                            <Input
                                className='inputSession'
                                onKeyUp={ this.validate }
                                ref='contentNewEmail'
                                reference='newEmail'
                                title='Correo Electrónico'
                                type='email' />
                            <Input
                                className='inputSession'
                                onKeyUp={ this.validate }
                                ref='contentNewPass'
                                reference='newPass'
                                title='Contraseña'
                                type='password' />
                            <RadioBtn
                                active={ this.activeRadioBtn }
                                content={obj}
                                name='sex'
                                ref='contentSex' />
                            <input
                                className={ active }
                                disabled={ !activeBtn }
                                type="submit"
                                value="REGISTRARSE" />
                        </form>
                        <Btn
                            text='REGRESAR'
                            className='Btn-default'
                            onClick={ this.home } />
                    </div>
                </div>
            </div>
        );
    }

    validate = () => {
        let validate = false;
        if (
            this.refs.contentNewPass.refs.newPass.value &&
            this.refs.contentNewEmail.refs.newEmail.value &&
            this.refs.contentNewUser.refs.newUser.value &&
            (this.refs.contentSex.refs.Hombre.checked || this.refs.contentSex.refs.Mujer.checked)
           ) {
            validate = true;
        }

        this.setState({
            activeBtn : validate,
        });
    }

    handleFormSubmit = e => {
        e.preventDefault();

        const {
            dispatch,
            history,
        } =  this.props;

        const radioBtn = this.refs.contentSex.refs.Hombre.checked ? 'male' : 'female';
        const payLoad = {
          name: this.refs.contentNewUser.refs.newUser.value,
          email: this.refs.contentNewEmail.refs.newEmail.value,
          password: this.refs.contentNewPass.refs.newPass.value,
          gender: radioBtn,
        };

        dispatch(
          registerUser(payLoad, () => {
            history.push("/home");
          })
        );
      };

    activeRadioBtn = (event) => {
        this.validate();
        this.setState({
            checkValue : event.currentTarget.value,
        });
    }

    home = () => {
        const { history } = this.props;
        history.push('/')
    }
}

export default connect(mapStateToProps)(Register);