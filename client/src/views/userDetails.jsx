import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import * as qs from 'query-string';
import Input from '../components/global/input';

import Header from '../components/global/header';
import Menu from '../components/menu';
import { getUserById } from '../redux/actions/index';

const mapStateToProps = state => ({
  userInformation: state.Users.userById,
  userInfo: state.getLogin.userInfo,
});

class UserDetails extends Component {
  state = {
    code: "",
  };

  componentWillMount() {
    const {
      location,
      history,
      dispatch,
    } = this.props;

    const parsed = qs.parse(location.search);

    if ( parsed && parsed.uid ) {
      dispatch(getUserById(parsed.uid));
    } else {
      history.push('/users');
    }

  }

  render() {
    const {
      userInformation,
    } = this.props;

    return (
      <Fragment>
        <Header { ...this.props } />
        <div className='dashBoard'>
          <div className='dashBoard-menu'>
            <Menu { ...this.props } />
          </div>
          <div className='dashBoard-content'>
            <h1>Detalles de Usuario</h1>
            <form onSubmit={this.handleFormSubmit} className='register-form'>
              <Input
                className='inputSession'
                title='Nombre Completo'
                type='text'
                disabled={ true }
                value={ userInformation && userInformation.name ? userInformation.name : '' } />
              <Input
                className='inputSession'
                title='Correo Electrónico'
                type='text'
                disabled={ true }
                value={ userInformation && userInformation.email ? userInformation.email : '' } />
              <Input
                className='inputSession'
                title='Tipo de Usuario'
                type='text'
                disabled={ true }
                value={ userInformation && userInformation.userType && userInformation.userType === '1' ? 'Administrador' : 'Usuario' } />
              <Input
                className='inputSession'
                title='Genero'
                type='text'
                disabled={ true }
                value={ userInformation && userInformation.gender && userInformation.gender === 'male' ? 'Hombre' : 'Mujer' } />
          </form>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps)(UserDetails);
