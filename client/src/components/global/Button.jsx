import React, { Component } from 'react';

class Button extends Component {
    render() {
        const {
            className,
            onClick,
            text,
        } = this.props;
        return (
            <div className='BtnWrapper'>
              <div onClick={ onClick } className={`Btn ${ className }`}>
                { text }
              </div>
            </div>
        );
    }
}

export default Button;