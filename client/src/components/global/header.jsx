import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { reAuthUser, logoutUser } from "../../redux/actions/index";
import Img from '../../img/photo.png';

const mapStateToProps = state => ({
  userInfo: state.getLogin.userInfo,
  isAuthed: state.getLogin.isAuthed,
  redirectLogOutUser: state.getLogin.redirectLogOutUser,
});

class Header extends Component {
    static defaultProps = {
        left: 0,
    };

    state = {
        active: false,
    };

    constructor() {
        super();
        this.handleAccount = this.handleAccount.bind(this);
    }

    componentWillMount() {
        const {
            dispatch,
        } = this.props;
        dispatch(reAuthUser());
    }

    componentWillReceiveProps(nextProps) {
        const {
            history,
            redirectLogOutUser,
        } = this.props;

        if ( ( redirectLogOutUser !== nextProps.redirectLogOutUser && nextProps.redirectLogOutUser) || nextProps.redirectLogOutUser ) {
            history.push('/');
        }
    }

    render() {
        const { active } = this.state;
        const {
            userInfo,
            dispatch,
            history,
        } = this.props;

        return (
            <div className='header'>
                <div className='header-content-logo'></div>
                <div className='header-content-menu'>
                    <div onClick={this.activeMenu} ref='menu' className='header-menu'>
                        <img
                            className='header-menu-imagen'
                            src={ userInfo && userInfo.profileImage ? userInfo.profileImage : Img }
                            alt='profile' />
                    </div>
                        <div className='header-content-user'>
                            <p className='header-content-welcome'>
                                { userInfo && userInfo.gender && userInfo.gender === 'male' ? 'Bienvenido' : 'Bienvenida' }
                            </p>
                            <p className='header-content-name '>{ userInfo.name }</p>
                        </div>
                    {
                        active  &&
                          <Fragment>
                            <div ref='list' onClick={ () => this.handleAccount() } className='header-menu-content'>
                                <div className='header-menu-option separation'>Mi cuenta</div>
                                <div
                                    className='header-menu-option'
                                    onClick={ () => dispatch(logoutUser(() => history.push("/"))) } >
                                    Cerrar Sesión
                                </div>
                            </div>
                          </Fragment>
                    }
                </div>
            </div>
        );
    }

    handleAccount() {
        const {
            history,
        } = this.props;

        history.push('/profile');
    }

    activeMenu = () => {
        const { active } = this.state;
        this.setState({
            active : !active,
        });
    }
}

export default connect(mapStateToProps)(Header);