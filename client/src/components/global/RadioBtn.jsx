import React, { Component } from 'react';

class RadioBtn extends Component {
    state = {

    }
    render() {
        const {
            active,
            content,
            name,
        } = this.props;

        return (
            <div className='RadioBtn'>
                {
                    content.map( (info, index) => {
                      return <div className='RadioBtn-wrapper' key={index}>
                        <span>{info.text}</span>
                        <input name={ name } onClick={ (e) => active(e) } ref={info.text} type="radio" value={info.text} />
                      </div>
                    })
                }
            </div>
        );
    }
}

export default RadioBtn;