import React, { Component } from 'react';

class Input extends Component {
    render(){
        const {
            className,
            onKeyUp,
            reference,
            title,
            type,
            value,
            disabled,
        } = this.props;
        return(
            <div className='componentInput'>
                <span className='input-title'>{ title }</span>
                <input
                disabled={ disabled }
                onKeyUp={ onKeyUp }
                ref={ reference }
                className={ `input ${className}` }
                type={ type }
                value={ value } />
            </div>
        );
    }
}

export default Input;