import React, { Component, Fragment } from 'react';
import { Chart } from "react-google-charts";

class ComponentChart extends Component {
    render() {
        const {
            chartType,
            /* colorTextH,
            colorTextV, */
            data,
            height,
            // minValue,
            title,
            titleH,
            titleV,
            width,
        } = this.props;

        return (
            <Fragment>
                {
                    chartType === 'Histogram' &&
                        <Chart
                        width={width}
                        height={height}
                        chartType="Histogram"
                        loader={<div>Loading Chart</div>}
                        data={data}
                        options={{
                            title: title,
                            legend: { position: 'none' },
                        }}
                        rootProps={{ 'data-testid': '1' }}/>
                }
                {
                    chartType === 'PieChart' &&
                    <Chart
                        width={width}
                        height={height}
                        chartType="PieChart"
                        loader={<div>Loading Chart</div>}
                        data={data}
                        options={{
                            title: title,
                        }}
                        rootProps={{ 'data-testid': '1' }} />
                }
                {
                    chartType === 'LineChart' &&
                    <Chart
                        width={width}
                        height={height}
                        chartType="LineChart"
                        loader={<div>Loading Chart</div>}
                        data={data}
                        options={{
                            hAxis: {
                            title: titleH,
                            },
                            vAxis: {
                            title: titleV,
                            },
                        }}
                        rootProps={{ 'data-testid': '1' }}/>
                }
            </Fragment>
        );
    }

}

export default ComponentChart;