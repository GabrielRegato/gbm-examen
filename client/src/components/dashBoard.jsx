import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { getChartInformation } from '../redux/actions/index';
import Chart from './global/chart';

const mapStateToProps = state => ({
    chartInformarion : state.chartInfo.chart,
});

class DashBoard extends Component {
    constructor(){
        super();
        this.state = {
            optionSelected : 'Histogram',
        };
    }
    
    componentWillMount() {
        const { dispatch } = this.props;
        dispatch(getChartInformation());
    }
    render() {
        const { chartInformarion } = this.props;
        const { optionSelected } = this.state;

        const obj = [];

        switch(optionSelected) {
            case 'Histogram':
            obj.push(['Year', 'Sales']);
            chartInformarion.map( element =>
                element.map( atributo => {
                    return obj.push([atributo.Fecha, atributo.Precio] );
                })
            );
            break;
            case 'PieChart':
            obj.push(['Fecha', 'Precio']);
            chartInformarion.map( element =>
                element.map( atributo => {
                    return obj.push([atributo.Fecha, atributo.Precio] );
                })
            );
            break;
            case 'LineChart':
            obj.push(['Year', 'Sales', 'Expenses']);
            chartInformarion.map( element =>
                element.map( atributo => {
                    return obj.push([atributo.Fecha, 0, atributo.Precio] );
                })
            );
            break;
            default:

        }
       

        const admin = [
            'Histogram',
            'PieChart',
            'LineChart',
        ];

        /* const user = [
            'Histogram',
            '',
        ]; */

        return (
            <Fragment>
                  <h1>Gráficas</h1>
                    <div className='dashBoard-select'>
                        <select className='chartSelect' onChange={ (e) => this.selected(e) }>
                           {admin.map( (element, index) =>
                                <option key={ index } value={ element }>{element}</option>
                            )}
                        </select>
                    </div>
                    {
                        chartInformarion.length > 0 &&
                            <Chart
                                colorTextH='#333'
                                colorTextV='#333'
                                chartType={ optionSelected }
                                data={obj}
                                height={ '300px' }
                                minValue={ 0 }
                                title={ 'Company Performance' }
                                titleH='Ejemplo2'
                                titleV='Ejemplo1'
                                width={ '600px' } />
                    }
            </Fragment>
        );
    }

    selected = event => {
        this.setState({
            optionSelected : event.currentTarget.value,
        });
    }
}

export default connect(mapStateToProps)(DashBoard);