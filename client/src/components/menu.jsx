import React, { Component } from 'react';
import cx from 'classnames';

class Menu extends Component {

    constructor(props) {
        super(props);

        this.handleChangeRoute = this.handleChangeRoute.bind(this);
    }

    render() {
        const {
            userInfo,
        } = this.props;

        const menuClass = cx({
            'menu-option' : true,
        });

        return(
            <div className='menu'>
                <div onClick={ () => this.handleChangeRoute('/home') } className={ menuClass }>
                    <span>Dashboard</span>
                </div>
                { userInfo && userInfo.userType && userInfo.userType === 1 ?
                <div onClick={ () => this.handleChangeRoute('/users') } className={ menuClass }>
                    <span>Control Users</span>
                </div> : null }
            </div>
        );
    }

    handleChangeRoute( route ) {
        const {
            history,
        } = this.props;

        history.push(route);
    }

}

export default Menu;