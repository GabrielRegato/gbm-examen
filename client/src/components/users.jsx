import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { getUsers, getUsersGoogle } from '../redux/actions/index';

const mapStateToProps = state => ({
    users : state.Users.users,
    usersGoogle : state.UsersGoogle.usersGoogle,
});

class Users extends Component {
    componentWillMount() {
        const { dispatch } = this.props;
        dispatch(getUsers());
        dispatch(getUsersGoogle());
    }
    render() {
        const { users, usersGoogle } = this.props;
        return(
            <Fragment>
                <h1>Google Users</h1>
                <table className='userGoogle'>
                    <thead>
                        <tr>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>googleId</th>
                        <th>Tipo Usuario</th>
                        </tr>
                    </thead>
                    <tbody>
                    { usersGoogle && usersGoogle.length > 0 && usersGoogle[0].map((item, key) => {
                        return (
                            <tr key={ key } >
                                <td>{key + 1 }</td>
                                <td>{item.name}</td>
                                <td>{item.email}</td>
                                <td>{item.googleId}</td>
                                <td>{item.userType === 1 ? 'Administrador' : 'Usuario' }</td>
                            </tr>
                        );
                    }) }
                    </tbody>
                </table>
                <h1>Local Users</h1>
                 <table className='user'>
                    <thead>
                        <tr>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Genero</th>
                        <th>Tipo Usuario</th>
                        <th>Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                        { users && users.length > 0 && users[0].map((item, key) => {
                            return (
                                <tr key={ key } >
                                    <td>{key + 1 }</td>
                                    <td>{item.name}</td>
                                    <td>{item.email}</td>
                                    <td>{item.gender}</td>
                                    <td>{item.userType === 1 ? 'Administrador' : 'Usuario' }</td>
                                    <td onClick={ () => this.handleUserDetails(item._id) } >{item._id}</td>
                                </tr>
                            );
                        }) }
                    </tbody>
                </table>
            </Fragment>
        );
    }

    handleUserDetails(idUser) {
        const {
            history,
        } = this.props;

        history.push({
            pathname: '/userDetails',
            search:  `?uid=${idUser}`,
        });
    }

}

export default connect(mapStateToProps)(Users);