import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LogIn from '../views/log-in';
import Register from '../views/register';
import NotFound from '../views/NotFound';
import Home from '../views/home';
import Acount from '../views/acount';
import SocialAuthRedirect from '../views/SocialAuthRedirect';
import Profile from '../views/profile';
import Users from '../views/users';
import UserDetails from '../views/userDetails';

const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact={true} path={ '/' }
          component= { LogIn } />
        <Route exact={true} path={ '/home' }
          component= { Home } />
        <Route exact={true} path={ '/acount' }
          component= { Acount } />
        <Route exact={true} path={ '/registro' }
          component= { Register } />
        <Route exact={true} path={ '/socialauthredirect' }
          component= { SocialAuthRedirect } />
        <Route exact={true} path={ '/profile' }
          component= { Profile } />
        <Route exact={true} path={ '/users' }
          component= { Users } />
        <Route exact={true} path={ '/userDetails' }
          component= { UserDetails } />
        <Route component= { NotFound } />
      </Switch>
    </BrowserRouter>
    );
};

export default Router;