var passport = require('passport');
var classdb = require('../class/connection');
var LocalStrategy = require('passport-local').Strategy;
var ObjectId = require('mongodb').ObjectId;

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  const userID = new ObjectId(id);
  classdb.buscaUsuario('user' , { '_id':  userID }, function(err, user) {
    done(err, user);
  });
});

passport.use(
  new LocalStrategy({
    usernameField: "email",
  },(username, password, done) => {
    classdb.buscaUsuario('user' , { 'email':  username }, function(err, user) {
      if (err) {
        return done(err);
      }

      if (!user) {
        return done(null, false, {
          email: "Email not found"
        });
      }

      if (!user.validPassword(password)) {
        return done(null, false, {
          password: "Password is wrong"
        });
      }

      return done(null, user);
    });
  })
);

module.exports = passport;