var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var classdb = require('../class/connection');
var User = require('../models/User');
var mongoose = require('mongoose');
var ObjectId = require('mongodb').ObjectId;

const GOOGLE_CLIENT_ID = '1036097436610-v6mq4vrsfgld9een3mqq5fehedrs1uh7.apps.googleusercontent.com';
const GOOGLE_CLIENT_SECRET = 'iFyZRaiLfrRI4ITUgQFpO00b';

passport.use(new GoogleStrategy({
	clientID: GOOGLE_CLIENT_ID,
	clientSecret: GOOGLE_CLIENT_SECRET,
	callbackURL: "/auth/google/callback"
}, function(accessToken, refreshToken, profile, done) {
    const obj = new User();
    obj._id = new mongoose.Types.ObjectId();
  	obj.accessToken = accessToken;
  	obj.refreshToken = refreshToken;
  	obj.googleId = profile.id;
  	obj.name = profile.displayName;
  	obj.gender = profile.gender;
  	obj.profileImage = profile.photos[0].value;
  	obj.email = profile.emails[0].value;
  	obj.userType = 1;

  	classdb.buscaUsuario('user' , { googleId: profile.id }, function(err, user) {
    		if (err) {
      		return done(err);
    		}

    		if (user) {
      		return done(null, obj);
    		} else {
      		classdb.insertaUsuarioLocal('user', obj, function(err, insert) {
        		if ( err ) {
          		return done(err);
        		}

            if ( insert ) {
          		return done(null, obj);
        		}
      	});
    	}
		});
	}
));

module.exports = passport;