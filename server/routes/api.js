var express = require('express');
var router = express.Router();
var  es6 = require('es6-promise').polyfill();
var fetch = require('isomorphic-fetch');
var classdb = require('../class/connection');

/* GET API listing. */

router.get('/chartsInfo', function(req, res, next) {
  const request = fetch('https://www.gbm.com.mx/Mercados/ObtenerDatosGrafico?empresa=IPC').then((response) => {
    return response.json();
  });

  return request.then(json => {
  	// console.log('json', json);
    res.json(json && json.resultObj ? json.resultObj : []);
  });

  // res.send('respond with a resource');
});

module.exports = router;
