var express = require('express');
var router = express.Router();
var passportGoogle = require('../auth/google');

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Please Sign In with:' });
});

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

router.get('/google',
  passportGoogle.authenticate('google', { scope: ['profile', 'email'] }));

router.get('/google/callback',
  passportGoogle.authenticate('google', { failureRedirect: '/auth/login' }),
  function(req, res) {
  	console.log('si hizo bien todo');
    res.redirect('/users');
  });

module.exports = router;