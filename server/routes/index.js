const express = require('express');
const router = express.Router();
const classdb = require('../class/connection');
const passport = require('../auth/passport');
const es6 = require('es6-promise').polyfill();
const fetch = require('isomorphic-fetch');
const jwt_secret = 's3cr3t';
const jwt = require("express-jwt");
const auth = jwt({
  secret: jwt_secret,
  requestProperty: "payload"
});
var ObjectId = require('mongodb').ObjectId;

// Controladores
const authController = require("../controllers/authController");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('indexLocal.ejs', { title: 'Express' });
});

router.get('/login', function(req, res, next) {
  res.render('loginLocal.ejs', { message: req.flash('loginMessage') });
});

router.get('/signup', function(req, res) {
  res.render('signupLocal.ejs', { message: req.flash('signupMessage') });
});

router.get('/profile', isLoggedIn, function(req, res) {
  res.render('profileLocal.ejs', { user: req.user });
});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

router.post('/signup', passport.authenticate('local-signup', {
  successRedirect: '/profile',
  failureRedirect: '/signup',
  failureFlash: true,
}));

// Routes for an external endpoint
router.get('/chartsInfo', function(req, res, next) {
  const request = fetch('https://www.gbm.com.mx/Mercados/ObtenerDatosGrafico?empresa=IPC').then((response) => {
    return response.json();
  });

  return request.then(json => {
    res.json(json && json.resultObj ? json.resultObj : []);
  });

});

// Routes for session
router.post("/login", authController.login);

router.post("/register", authController.register);

// Google AUTH
router.get("/auth/google", authController.googleAuth);

router.get("/auth/google/callback", authController.googleAuthCallBack);

// Routes for listUsers
router.get('/listUsers', function(req, res, next) {
  const fields = {
    '_id': 1,
    'email': 1,
    'gender': 1,
    'name': 1,
    'profileImage': 1,
    'userType': 1,
  };
  classdb.listaUsuarios('user', {'googleId': { "$exists" : false }}, fields, function(err, items) {
    if ( err ) {
      res.jsonp(err);
    } else {
      res.jsonp(items);
    }
  });
});

router.get('/listUsersGoogle', function(req, res, next) {
  const fields = {
    '_id': 1,
    'email': 1,
    'gender': 1,
    'googleId': 1,
    'name': 1,
    'profileImage': 1,
    'userType': 1,
  };
  classdb.listaUsuarios('user', {'googleId': { "$exists" : true , "$ne": null}}, fields, function(err, items) {
    if ( err ) {
      res.jsonp(err);
    } else {
      res.jsonp(items);
    }
  });
});


router.get('/userById', function(req, res, next) {

  const {
    uid,
  } = req.query;

  const fields = {
    '_id': 1,
    'email': 1,
    'gender': 1,
    'name': 1,
    'profileImage': 1,
    'userType': 1,
  };

  const userID = new ObjectId(uid);
  classdb.buscaUsuarioById('user' , { '_id':  userID }, fields, function(err, user) {
    if ( err ) {
      res.jsonp(err);
    } else {
      res.jsonp(user);
    }
  });
});

module.exports = router;

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated())
      return next();
  res.redirect('/');
}