const passport = require("passport");
const mongoose = require("mongoose");
var UserLocal = require('../models/UserLocal');
const jwt = require("jsonwebtoken");
const jwt_secret = 's3cr3t';
var classdb = require('../class/connection');

module.exports.register = (req, res, next) => {
  const userInfo = req.body;
  const userLocal = new UserLocal();
  userLocal._id = new mongoose.Types.ObjectId();
  userLocal.name = userInfo.name;
  userLocal.email = userInfo.email;
  userLocal.gender = userInfo.gender;
  userLocal.password = userLocal.generateHash(userInfo.password);
  userLocal.userType = 2;

  classdb.buscaUsuario('user' , { 'email':  userInfo.email }, function(err, user) {
    if (err) {
      res.status(401).json(err);
    }

    if (user) {
      res.status(401).json('That email is already in use.');
    } else {
      classdb.insertaUsuarioLocal('user', userLocal, function(err, insert) {
        if ( err ) {
          res.status(401).json(err);
        }

        if ( insert ) {
          module.exports.login(req,res);
        }
      });
    }
  });
};

module.exports.login = (req, res) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) {
      res.status(401).json(err);
      return;
    }

    if (user) {
      const token = user.generateJwt();
      res.status(200).json({
        userInfo: user,
        token: token
      });
    } else {
      res.status(401).json(info);
    }
  })(req, res);
};

module.exports.googleAuth = (req, res, next) => {
  passport.authenticate("google", {
    scope: ["profile", "email"],
    state: req.query.linkinguri
  })(req, res, next);
};

module.exports.googleAuthCallBack = (req, res, next) => {
  passport.authenticate("google", (err, user, info) =>
    generateTokenAndRedirect(req, res, next, err, user, info)
  )(req, res, next);
};

const generateTokenAndRedirect = (req, res, next, err, user, info) => {
  if (err) {
    return next(err);
  }
  if (user) {
    const token = user.generateJwt();
    return res.redirect(`${req.query.state}?token=${token}`);
  } else {
    return res.redirect("${req.query.state}");
  }
};