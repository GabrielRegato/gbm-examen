var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var dbase = '';
var mongobase = 'mongodb://localhost:27017/gbmapp';
var Q = require('q');
var mongoose = require('mongoose');
var User = require('../models/User');
var UserLocal = require('../models/UserLocal');

mongoose.Promise = global.Promise;
var conexion = function(urlDB) {
	var d = Q.defer();
  	MongoClient.connect(urlDB, { useNewUrlParser: true }, function(error, db) {
    	if (error) {
      		return d.reject(error);
          console.log('conexión no exitosa', error);
		} else {
    		console.log('conexión exitosa');
      		dbase = db.db('gbmapp');
      		d.resolve(dbase);
    	}
  	});
  	return d.promise;
};

var promesa_conexion = conexion(mongobase);

module.exports = {
	buscaUsuario : function(coleccion, criterio, callback) {
    	promesa_conexion.done(function() {
      		var colect= dbase.collection(coleccion);
      		colect.findOne(criterio, function (error, user) {
				if (error) {
	  				return callback(error, false);
				} else{
					if (user) {
						const obj = new UserLocal(user);
						return callback(error, obj);
					} else {
						return callback(null, false);
					}
				}
			});
		});
	},

  buscaUsuarioById : function(coleccion, criterio, campos, callback) {
      promesa_conexion.done(function() {
          var colect= dbase.collection(coleccion);
          colect.find(criterio).project(campos).toArray(function (error, user) {
        if (error) {
            return callback(error, false);
        } else{
          if (user) {
            return callback(error, user[0]);
          } else {
            return callback(null, false);
          }
        }
      });
    });
  },

	insertaUsuarioLocal : function(coleccion, objeto, callback) {
    	promesa_conexion.done(function() {
    		var colect= dbase.collection(coleccion);
      		colect.insertOne(objeto, function(errInsert, inserta) {
				if (errInsert) {
	  				return callback(errInsert, false);
				} else{
	  				return callback(errInsert, true);
				}
      		});
    	});
	},

  listaUsuarios : function(coleccion, criterio, campos, callback){
    promesa_conexion.done(function(){
      var colect= dbase.collection(coleccion);
      colect.find(criterio).project(campos).toArray(function(error, items) {
        if (error) {
          return callback(error, false);
        }
        if (items) {
          return callback(null, items);
        }
      });
    });
  },

  actualizaUsuario : function(coleccion, criterio, set, callback) {
    promesa_conexion.done(function(){
      var colect= dbase.collection(coleccion);
      colect.update(criterio, {$set: set}, {w:1}, function(error, result) {
        if (error) {
          return callback(error, false);
        } else {
          return callback(null, true);
        }
      });
    });
  },
};