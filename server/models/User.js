var mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const jwt_secret = 's3cr3t';

var UserSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  accessToken: String,
  email: String,
  gender: String,
  googleId: String,
  name: String,
  profileImage: String,
  refreshToken: String,
  userType: Number,
  updated_at: { type: Date, default: Date.now },
});

UserSchema.methods.generateJwt = function() {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign(
    {
      _id: this._id,
      email: this.email,
      exp: parseInt(expiry.getTime() / 1000),
      gender: this.gender,
      googleId: this.googleId,
      name: this.name,
      profileImage: this.profileImage,
      userType: this.userType,
    },
    jwt_secret
  );
};

module.exports = mongoose.model('User', UserSchema);