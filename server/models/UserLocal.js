var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
const jwt = require("jsonwebtoken");
const jwt_secret = 's3cr3t';

var UserLocalSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: String,
  gender: String,
  name: String,
  profileImage: String,
  userType: Number,
  password: String,
  updated_at: { type: Date, default: Date.now },
});

UserLocalSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserLocalSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

UserLocalSchema.methods.generateJwt = function() {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);
  return jwt.sign(
    {
      _id: this._id,
      email: this.email,
      exp: parseInt(expiry.getTime() / 1000),
      gender: this.gender,
      name: this.name,
      profileImage: this.profileImage,
      userType: this.userType,
    },
    jwt_secret
  );
};

module.exports = mongoose.model('UserLocal', UserLocalSchema);